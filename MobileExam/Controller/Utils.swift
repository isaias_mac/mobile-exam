//
//  Utils.swift
//  MobileExam
//
//  Created by Isaías on 12/16/17.
//  Copyright © 2017 Isaías. All rights reserved.
//

import Foundation
import UIKit
import EventKit

class Utils: NSObject {
    
    static func convertStrToDate(dateStr: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DATE_FORMAT
        
        return dateFormatter.date(from: dateStr)!
    }
    
    static func saveCalendarEvent(match: Match, completeSuccess: @escaping () -> Void, completeFailed: @escaping () -> Void){
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: EKEntityType.event, completion:
            {(granted, error) in
                if !granted {
                    print("Access to store not granted")
                    completeFailed()
                }
                else{
                    let newEvent = EKEvent(eventStore: eventStore)
                    newEvent.calendar = eventStore.defaultCalendarForNewEvents
                    newEvent.title = Match.getNameEventCalendar(match)
                    newEvent.startDate = match.startTime! as Date
                    let calendar = Calendar.current
                    let newDate = calendar.date(byAdding: .hour, value: 2, to: match.startTime! as Date)
                    newEvent.endDate = newDate
                    
                    do{
                        try eventStore.save(newEvent, span: .thisEvent, commit: true)
                        completeSuccess()
                    }
                    catch{
                        print("Error al guardar en el Calendario \(error)")
                        completeFailed()
                    }
                }
        })
    }
    
    static func showPermissionError(vc: UIViewController, title: String, message: String){
        let alertController = UIAlertController(title: Constants.SETTINGS_LABEL, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: title, style: .default) { (alertAction) in
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(appSettings)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: Constants.CANCEL_LABEL, style: .cancel)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true)
    }
    
    static func showAlertTryAgain(title: String, message: String, vc: UIViewController, closureTryAgain: @escaping () -> Void){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: Constants.CANCEL_LABEL, style: .cancel)
        
        let tryAgainAction = UIAlertAction(title: Constants.TRY_AGAIN_LABEL, style: .default) { action in
            closureTryAgain()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(tryAgainAction)
        
        vc.present(alertController, animated: true)
    }
    
}
