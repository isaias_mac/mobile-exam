//
//  MatchesTVC.swift
//  MobileExam
//
//  Created by Isaías on 12/16/17.
//  Copyright © 2017 Isaías. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import SwiftyJSON
import SDWebImage


class MatchesTVC: UITableViewController {

    let dataDict : [Dictionary<String, String>] = []
    var matches : [Match] = []
    let coreDataManager = CoreDataManager()
    let tableRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false
        self.tableView.backgroundColor = UIColor.white
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = tableRefreshControl
        }
        else{
            self.tableView.addSubview(tableRefreshControl)
        }
        
        self.tableRefreshControl.tintColor = Constants.BLUE_COLOR
        self.tableRefreshControl.addTarget(self, action: #selector(callAPI), for: .valueChanged)
        
        self.addEmptyView()
        
        matches = Match.fetchMatches(context: coreDataManager.managedObjectContext)
        
        callAPI()
        
    }
    
    // MARK: - Methods
    
    @objc func callAPI() {
        if !self.tableRefreshControl.isRefreshing{
            HUD.show(.progress)
        }
        
        Alamofire.request(Constants.API_URL).responseJSON { response in
            
            if response.response?.statusCode != 200{
                HUD.hide()
                Utils.showAlertTryAgain(title: "Error", message: Constants.MESSAGE_HTTP_ERROR, vc: self, closureTryAgain: {
                    self.callAPI()
                })
                self.addEmptyView()
            }
            else{
                if let json = response.data {
                    do{
                        let data = try JSON(data: json)
                        print(data)
                        if let itemsArray = data[Constants.JSON_items].array{
                            for item in itemsArray{
                                Match.saveMatch(context: self.coreDataManager.managedObjectContext, data: item)
                            }
                            
                            if itemsArray.count > 0{
                                self.matches = Match.fetchMatches(context: self.coreDataManager.managedObjectContext)
                                print(self.matches)
                                self.tableView.reloadData()
                                self.tableView.backgroundView = UIView()
                                self.tableView.isScrollEnabled = true
                            }
                            
                            if self.tableRefreshControl.isRefreshing{
                                self.tableRefreshControl.endRefreshing()
                            }
                            
                            HUD.hide()
                        }
                    }
                    catch{
                        HUD.hide()
                        Utils.showAlertTryAgain(title: "Error", message: Constants.MESSAGE_HTTP_ERROR, vc: self, closureTryAgain: {
                            self.callAPI()
                        })
                        self.addEmptyView()
                    }
                    
                }
            }
        }
    }
    
    func addEmptyView() {
        let emptyView = UIView()
        emptyView.frame = CGRect(x: 5, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height)
        
        let messageLabel = UILabel()
        messageLabel.frame = CGRect(x: 10, y: self.view.center.y - 100, width: self.tableView.frame.width, height: 100)
        messageLabel.text = Constants.NO_MATCHES_AVAILABEL_LABEL
        messageLabel.textAlignment = NSTextAlignment.center
        
        let reloadButton = UIButton()
        reloadButton.frame = CGRect(x: self.view.center.x - 150/2, y: self.view.center.y, width: 150, height: 50)
        reloadButton.backgroundColor = Constants.BLUE_COLOR
        reloadButton.setTitle(Constants.RELOAD_LABEL, for: .normal)
        reloadButton.layer.cornerRadius = 5.0
        reloadButton.layer.borderWidth = 1.0
        reloadButton.layer.borderColor = Constants.BLUE_COLOR.cgColor
        reloadButton.layer.masksToBounds = true
        reloadButton.addTarget(self, action: #selector(self.handleReloadButton(_:)), for: .touchUpInside)
        
        emptyView.addSubview(messageLabel)
        emptyView.addSubview(reloadButton)
        
        self.tableView.backgroundView = emptyView
        self.tableView.separatorStyle = .none
        self.tableView.isScrollEnabled = false
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    @objc func handleReloadButton(_ sender: UIButton) {
        callAPI()
    }
    
    @objc func handleAddCalendar(_ sender: MatchButton) {
        let id = sender.strId!
        
        let (result, matchObject) = Match.fetchById(context: coreDataManager.managedObjectContext, id: id)
        
        if result == true {
            let match = matchObject as! Match
            HUD.show(.progress)
            
            Utils.saveCalendarEvent(match: match, completeSuccess: {
                DispatchQueue.main.async {
                    HUD.hide()
                    HUD.flash(.success, delay: 0.8)
                }
            }, completeFailed: {
                HUD.hide()
                DispatchQueue.main.async{
                    Utils.showPermissionError(vc: self, title: Constants.CONTINUE_LABEL, message: Constants.PERMISSION_CALENDAR_ERROR)
                }
            })
        }
        else{
            HUD.hide()
            DispatchQueue.main.async{
                Utils.showPermissionError(vc: self, title: Constants.CONTINUE_LABEL, message: Constants.PERMISSION_CALENDAR_ERROR)
            }
        }
    }
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matches.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MatchCell
        
        let dataItem = matches[indexPath.item] as Match
        cell.competitionNameLabel.text = dataItem.competitionName
        cell.localTeamNameLabel.text = dataItem.localTeamName
        cell.visitTeamNameLabel.text = dataItem.visitTeamName
        cell.localTeamImg.sd_setImage(with: URL(string: dataItem.localTeamImg!), completed: nil)
        cell.visitTeamImg.sd_setImage(with: URL(string: dataItem.visitTeamImg!), completed: nil)
        cell.localGoalsLabel.text = String(describing: dataItem.localGoals)
        cell.visitGoalsLabel.text = String(describing: dataItem.visitGoals)
        cell.locationAndTimeLabel.text = Match.getLocationAndTime(dataItem)//dataItem.getLocationAndTime()
        cell.calendarButton = cell.calendarButton as MatchButton
        cell.calendarButton.strId = dataItem.id
        cell.calendarButton.addTarget(self, action: #selector(handleAddCalendar(_:)), for: .touchUpInside)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red:0.13, green:0.22, blue:0.48, alpha:1.0)
        
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: 50)
        titleLabel.text = Constants.TITLE_MATCHES.uppercased()
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = NSTextAlignment.center
        headerView.addSubview(titleLabel)
        
        return headerView
    }

}
