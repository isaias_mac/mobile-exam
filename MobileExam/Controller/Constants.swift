//
//  Constants.swift
//  MobileExam
//
//  Created by Isaías on 12/16/17.
//  Copyright © 2017 Isaías. All rights reserved.
//

import UIKit

struct Constants {
    static let API_URL = "http://futbol.masfanatico.cl/api/u-chile/match/in_competition/transicion2017"
    
    static let DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
    
    static let BLUE_COLOR = UIColor(red:0.13, green:0.22, blue:0.48, alpha:1.0)
    static let RED_COLOR = UIColor(red:0.76, green:0.15, blue:0.12, alpha:1.0)
    
    static let TITLE_MATCHES = NSLocalizedString("Lista de Partidos", comment: "")
    static let REMEMBER_LABEL = NSLocalizedString("Recordar", comment: "")
    static let SETTINGS_LABEL = NSLocalizedString("Configuración", comment: "")
    static let CONTINUE_LABEL = NSLocalizedString("Continuar", comment: "")
    static let CANCEL_LABEL = NSLocalizedString("Cancelar", comment: "")
    static let TRY_AGAIN_LABEL = NSLocalizedString("Reintentar", comment: "")
    static let NO_MATCHES_AVAILABEL_LABEL = NSLocalizedString("No hay partidos disponibles", comment: "")
    static let RELOAD_LABEL = NSLocalizedString("Recargar", comment: "")
    
    static let MESSAGE_HTTP_ERROR = NSLocalizedString("Ha ocurrido un error, intentalo de nuevo más tarde.", comment: "")
    static let PERMISSION_CALENDAR_ERROR = NSLocalizedString("Ha ocurrido un error al guardar. Verifica los permisos para el Calendario. Para habilitarlos presiona en Continuar", comment:"")
    
    static let JSON_items = "items"
    static let JSON_id = "id"
    static let JSON_local_team = "local_team_name_s"
    static let JSON_competition_name = "competition_name_s"
    static let JSON_stadium_name = "stadium_name_s"
    static let JSON_local_goals = "local_goals_i"
    static let JSON_visit_goals = "visit_goals_i"
    static let JSON_visit_team = "visit_team_name_s"
    static let JSON_local_team_img = "local_team_image_team-icon_url_s"
    static let JSON_visit_team_img = "visit_team_image_team-icon_url_s"
    static let JSON_start_time = "start_time_dt"

}
