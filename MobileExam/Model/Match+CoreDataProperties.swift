//
//  Match+CoreDataProperties.swift
//  MobileExam
//
//  Created by Isaías on 12/19/17.
//  Copyright © 2017 Isaías. All rights reserved.
//
//

import Foundation
import CoreData


extension Match {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Match> {
        return NSFetchRequest<Match>(entityName: "Match")
    }

    @NSManaged public var competitionName: String?
    @NSManaged public var id: String?
    @NSManaged public var stadiumName: String?
    @NSManaged public var localTeamName: String?
    @NSManaged public var visitTeamName: String?
    @NSManaged public var localTeamImg: String?
    @NSManaged public var visitTeamImg: String?
    @NSManaged public var localGoals: Int16
    @NSManaged public var visitGoals: Int16
    @NSManaged public var startTime: NSDate?

}
