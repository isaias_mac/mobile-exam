//
//  Match+CoreDataClass.swift
//  MobileExam
//
//  Created by Isaías on 12/19/17.
//  Copyright © 2017 Isaías. All rights reserved.
//
//

import Foundation
import CoreData
import SwiftyJSON

@objc(Match)
public class Match: NSManagedObject {
    
    static func saveMatch(context: NSManagedObjectContext, data: JSON){
        let id = data[Constants.JSON_id].stringValue
        
        let result = self.fetchById(context: context, id: id)
        
        if result.status == false{
            let match = NSEntityDescription.insertNewObject(forEntityName: "Match", into: context) as! Match
            match.id = id
            match.competitionName = data[Constants.JSON_competition_name].stringValue
            match.stadiumName = data[Constants.JSON_stadium_name].stringValue
            match.localTeamName = data[Constants.JSON_local_team].stringValue
            match.visitTeamName = data[Constants.JSON_visit_team].stringValue
            match.localTeamImg = data[Constants.JSON_local_team_img].stringValue
            match.visitTeamImg = data[Constants.JSON_visit_team_img].stringValue
            match.startTime = Utils.convertStrToDate(dateStr: data[Constants.JSON_start_time].stringValue) as NSDate
            match.localGoals = data[Constants.JSON_local_goals].int16!
            match.visitGoals = data[Constants.JSON_visit_goals].int16!
            do{
                try context.save()
            }
            catch{
                fatalError("Failure to save context: \(error)")
            }
        }
    }
    
    static func fetchById(context: NSManagedObjectContext, id: String) -> (status: Bool, match: Any?){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Match")
        request.predicate = NSPredicate(format: "\(Constants.JSON_id) == %@", id)
        request.fetchLimit = 1
        
        do {
            let result = try context.fetch(request)
            
            if result.count > 0 {
                return (status: true, match: result.first as! Match)
            }
        } catch {
            print("Failed")
        }
        
        return (status: false, match: nil)
    }
    
    static func saveMatch(context: NSManagedObjectContext, data: NSDictionary) -> Bool {
        let match = NSEntityDescription.insertNewObject(forEntityName: "Match", into: context) as! Match
        match.id = data.value(forKey: Constants.JSON_id) as? String
        match.competitionName = data.value(forKey: Constants.JSON_competition_name) as? String
        match.stadiumName = data.value(forKey: Constants.JSON_stadium_name) as? String
        match.localTeamName = data.value(forKey: Constants.JSON_local_team) as? String
        match.visitTeamName = data.value(forKey: Constants.JSON_visit_team) as? String
        match.localTeamImg = data.value(forKey: Constants.JSON_local_team_img) as? String
        match.visitTeamImg = data.value(forKey: Constants.JSON_visit_team_img) as? String
        match.startTime = data.value(forKey: Constants.JSON_start_time) as? NSDate
        match.localGoals = data.value(forKey: Constants.JSON_local_goals) as! Int16
        match.visitGoals = data.value(forKey: Constants.JSON_visit_goals) as! Int16
        
        do{
            try context.save()
            return true;
        }
        catch{
            fatalError("Failure to save context: \(error)")
        }
        
        return false
    }
    
    static func fetchMatches(context: NSManagedObjectContext) -> [Match]{
        let matchFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Match")
        
        do {
            let fetchedMatches = try context.fetch(matchFetch) as! [Match]
            return fetchedMatches
        } catch {
            fatalError("Failed to fetch matches: \(error)")
        }
    }
    
    static func getNameEventCalendar(_ match: Match) -> String {
        return "\(String(describing: match.localTeamName!)) vs \(String(describing: match.visitTeamName!))"
    }
    
    static func getFullDateMatch(_ match: Match) -> String {
        let date = match.startTime
        
        // Devuelve mes en String (Jul, Ago) y Lozalized
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMM")
        let monthInitial = df.string(from: date! as Date)
        let calendar = Calendar.current
        
        let day = calendar.component(.day, from: date! as Date)
        let hour = calendar.component(.hour, from: date! as Date)
        
        return "\(day) de \(monthInitial) - \(hour)hrs"
    }
    
    static func getLocationAndTime(_ match: Match) -> String {
        let fullDateMatch = self.getFullDateMatch(match)
        return "\(fullDateMatch) - \(String(describing: match.stadiumName!))"
    }
    
}
