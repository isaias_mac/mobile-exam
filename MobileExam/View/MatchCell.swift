//
//  MatchCell.swift
//  MobileExam
//
//  Created by Isaías on 12/16/17.
//  Copyright © 2017 Isaías. All rights reserved.
//

import UIKit

class MatchCell: UITableViewCell {

    @IBOutlet weak var competitionNameLabel: UILabel!
    @IBOutlet weak var locationAndTimeLabel: UILabel!
    @IBOutlet weak var localTeamImg: UIImageView!
    @IBOutlet weak var visitTeamImg: UIImageView!
    @IBOutlet weak var localTeamNameLabel: UILabel!
    @IBOutlet weak var visitTeamNameLabel: UILabel!
    @IBOutlet weak var localGoalsLabel: UILabel!
    @IBOutlet weak var visitGoalsLabel: UILabel!
    @IBOutlet weak var calendarButton: MatchButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.white
        competitionNameLabel.textColor = Constants.BLUE_COLOR
        self.localGoalsLabel.textColor = Constants.RED_COLOR
        self.visitGoalsLabel.textColor = Constants.RED_COLOR
        self.calendarButton.setTitle(Constants.REMEMBER_LABEL, for: .normal)
        self.calendarButton.tintColor = Constants.BLUE_COLOR
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0).cgColor
        self.layer.cornerRadius = 4.0
        self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 10))
        contentView.frame = fr
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
