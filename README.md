# Practical exam

This repository has the purpose to give a "mission" for the Funx company's candidates. Evaluating technical capabilities, problem solving, git management and agility.

## Minimum objectives

* Integrate a list of matches.
* Save the specific match in the phone calendar.

You're free to develop any another features and changes. 

## Layout

We recommend this layout, but you can use anyone. Feel free to make the best.

![Alt text](layout.png)

## API

```
http://futbol.masfanatico.cl/api/u-chile/match/in_competition/transicion2017
```

## IOS Rules

* Swift 3 or 4
* You can use cocoapods

## Android Rules

* Java or Kotlin
* You can use graddle

## Send us

After than you finish your development you have to make a pull request in this repository. This one will be review for us and then will be rejected.








